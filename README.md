Automatic Renewal of Let's Encrypt Domains for GitLab Pages: OVH DNS Provider
===========================================================

This repository contains everything that is needed to use GitLab CI to renew SSL Certificates issued by Let's Encrypt,
for domains managed by OVH.

For each domain associated to GitLab Pages, define a scheduled job with the following environment variables:

- `OVH_ENDPOINT` the OVH API endpoint (`ovh-eu` or `ovh-ca`)
- `OVH_APP_KEY` the OVH application key
- `OVH_APP_SECRET` the OVH application secret
- `OVH_CONSUMER_KEY` the OVH consumer key
- `DOMAIN_NAME` the domain name to generate/renew certificates for
- `CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN` the GitLab API [personal access token](https://docs.gitlab.com/ee/api/README.html#personal-access-tokens)
- `GITLAB_PROJECT_ID` the GitLab project ID (username/repo). Make sure it is [URL encoded!](https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding)

The job will do the following:
- generate a new certificate using DNS validation
- update the certificate and key associated to the domain in the relevant repo (the one that serves pages under this domain)

OVH API keys can be generated [here](https://eu.api.ovh.com/createApp/),
or follow [this](https://docs.ovh.com/gb/en/customer/first-steps-with-ovh-api/).

For more information on the OVH DNS plugin, look [here](https://certbot-dns-ovh.readthedocs.io/en/stable/).
